#include "MemoryBlock.hpp"
#include <stdlib.h>
#include <exception>
#include <new>
#include "Logging.hpp"

MemoryBlock::MemoryBlock()
{
    BOOST_LOG_TRIVIAL(debug) << "MemoryBlock()";
}

MemoryBlock::MemoryBlock(size_t size)
{
    m_size = size;
    m_data = malloc(m_size);
    if (m_data == NULL) {
        throw std::bad_alloc();
    }

    BOOST_LOG_TRIVIAL(debug) << "MemoryBlock(" << size << ")";
}

void MemoryBlock::CleanUp()
{
    if (m_data != NULL) {
        BOOST_LOG_TRIVIAL(debug) << "MemoryBlock::CleanUp()";
        free(m_data);
        m_data = NULL;
    }
}

MemoryBlock::~MemoryBlock()
{
    BOOST_LOG_TRIVIAL(debug) << "MemoryBlock::~MemoryBlock()";
    this->CleanUp();
}
