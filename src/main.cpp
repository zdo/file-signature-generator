#include <iostream>

#include "Logging.hpp"
#include "Options.hpp"
#include "SignatureGeneratorFactory.hpp"
#include "SignatureFileGenerator.hpp"

int main(int argc, char **argv)
{
    try {
        auto options = Options::Parse(argc, argv);

        SignatureFileGenerator fileGenerator(options.inputFilePath, options.signatureFilePath);
        fileGenerator.SetBlockSize(options.blockSize);
        fileGenerator.SetThreadsCount(options.threadsCount);

        auto generator = SignatureGeneratorFactory::Create(options.hashFunction);
        fileGenerator.SetSignatureGenerator(generator);

        fileGenerator.Process();

        // UNIX-way is to keep silence by default on success.
    } catch (const std::exception &exc) {
        BOOST_LOG_TRIVIAL(error) << "Error: " << exc.what();
    } catch (...) {
        BOOST_LOG_TRIVIAL(error) << "Unknown error occured";
    }
}