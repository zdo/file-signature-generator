#ifndef SIGNATURE_GENERATOR
#define SIGNATURE_GENERATOR

#include "MemoryBlock.hpp"
#include <memory>

// Abstract class. Need to be overridden for specific hash algo.
class SignatureGenerator {
public:
    // Depends on algorithm that lies inside.
    virtual size_t GetRequestedOutputBlockSize() = 0;

    virtual void Process(std::shared_ptr<MemoryBlock> input,
        std::shared_ptr<MemoryBlock> output) = 0;
};

#endif
