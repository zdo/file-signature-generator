#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include <string>

// Application global options.
struct Options {
    static Options Parse(int argc, char **argv);

    std::string inputFilePath, signatureFilePath;
    size_t blockSize = 0;
    bool verbose = false;
    size_t threadsCount = 0; // 0 means taking the default value from system
    std::string hashFunction;
};

#endif
