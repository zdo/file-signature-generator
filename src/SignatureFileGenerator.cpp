#include "SignatureFileGenerator.hpp"

#include "Logging.hpp"
#include <iostream>
#include <thread>
#include <stdexcept>
#include <math.h>
#include <chrono>

SignatureFileGenerator::SignatureFileGenerator(const std::string &inputFile, const std::string &outputFile)
{
    m_inputFilePath = inputFile;
    m_outputFilePath = outputFile;
}

void SignatureFileGenerator::Process()
{
    BOOST_LOG_TRIVIAL(info) << "Processing started...";

    try {
        auto t0 = std::chrono::high_resolution_clock::now();

        this->PrepareToProcess();
        this->ProcessWithThreads();
        this->FinalizeProcessing();

        auto t1 = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> dt = t1 - t0;
        BOOST_LOG_TRIVIAL(info) << "The job took " << dt.count() << " seconds";
    } catch (...) {
        BOOST_LOG_TRIVIAL(debug) << "Error occured during processing";
        this->FinalizeProcessing();
        throw;
    }
}

size_t SignatureFileGenerator::GetRecommendedThreadsCount() const
{
    size_t coresCount = std::thread::hardware_concurrency();
    if (coresCount == 0) {
        coresCount = 2;
        BOOST_LOG_TRIVIAL(warning) << "Can't detect CPU cores count, will use "
            << coresCount << " by default";
    }
    return coresCount;
}

size_t SignatureFileGenerator::GetTotalBlocksCount() const
{
    size_t totalBlocksCount = ceil((double)m_inputFile->GetTotalSize() / m_blockSize);
    return totalBlocksCount;
}

void SignatureFileGenerator::PrepareToProcess()
{
    // Check generator.
    if (m_signatureGenerator == nullptr) {
        throw std::runtime_error("Specific signature generator must be set");
    }

    // Input file.
    m_inputFile = std::make_shared<File>(m_inputFilePath, File::Access_ReadOnly);

    // Output file.
    const size_t totalBlocksCount = this->GetTotalBlocksCount();
    const size_t outputBlockSize = m_signatureGenerator->GetRequestedOutputBlockSize();
    const size_t outputFileSize = totalBlocksCount * outputBlockSize;
    m_outputFile = std::make_shared<File>(m_outputFilePath, File::Access_ReadWrite,
        File::TruncateMode_Yes, outputFileSize);

    BOOST_LOG_TRIVIAL(debug) << "totalBlocksCount=" << totalBlocksCount;
}

void SignatureFileGenerator::ProcessWithThreads()
{
    std::vector<std::thread> threads;

    size_t threadsCount = m_threadsCount;
    if (threadsCount == 0) {
        threadsCount = this->GetRecommendedThreadsCount();
    }
    BOOST_LOG_TRIVIAL(debug) << "threadsCount=" << threadsCount;

    for (size_t i = 0; i < threadsCount; ++i) {
        auto t = std::thread(&SignatureFileGenerator::ThreadFn, this, i, threadsCount);
        threads.push_back(std::move(t));
    }

    for (size_t i = 0; i < threadsCount; ++i) {
        threads[i].join();
    }
}
void SignatureFileGenerator::FinalizeProcessing()
{
    m_outputFile.reset();
    m_inputFile.reset();
}

void SignatureFileGenerator::ThreadFn(size_t index, size_t totalThreadsCount)
{
    BOOST_LOG_TRIVIAL(debug) << "Thread " << index << " is here";

    const size_t totalBlocksCount = this->GetTotalBlocksCount();

    for (size_t blockIndex = index;
            blockIndex < totalBlocksCount;
            blockIndex += totalThreadsCount)
    {
        BOOST_LOG_TRIVIAL(debug) << "Will process block " << blockIndex
            << " in thread " << index;

        // Get input block.
        size_t inputOffset = blockIndex * m_blockSize;
        size_t inputSize = m_blockSize;
        bool isLastBlock = (blockIndex == (totalBlocksCount - 1));
        if (isLastBlock) {
            size_t totalSizeWithoutLastBlock = (totalBlocksCount - 1) * m_blockSize;
            size_t fixedInputSize = m_inputFile->GetTotalSize() - totalSizeWithoutLastBlock;
            if (fixedInputSize != inputSize) {
                BOOST_LOG_TRIVIAL(debug) << "Correct input block size for the last one from "
                    << inputSize << " to " << fixedInputSize << " bytes";
                inputSize = fixedInputSize;
            }
        }
        auto inputBlock = m_inputFile->MapToMemory(inputOffset, inputSize,
            MemoryMappedBlock::Access_Read);

        // Get output block.
        size_t outputSize = m_signatureGenerator->GetRequestedOutputBlockSize();
        size_t outputOffset = blockIndex * outputSize;
        auto outputBlock = m_outputFile->MapToMemory(outputOffset, outputSize,
            MemoryMappedBlock::Access_ReadWrite);

        // Generate the signature.
        BOOST_LOG_TRIVIAL(debug) << "Will generate signature for block " << blockIndex;
        m_signatureGenerator->Process(inputBlock, outputBlock);
        outputBlock->Sync();
        BOOST_LOG_TRIVIAL(debug) << "Done for block " << blockIndex;
    }

    BOOST_LOG_TRIVIAL(debug) << "Thread " << index << " is done";
}