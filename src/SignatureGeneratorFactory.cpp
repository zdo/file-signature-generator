#include "SignatureGeneratorFactory.hpp"

#include "SignatureGenerators/SignatureGeneratorMD5.hpp"
#include "SignatureGenerators/SignatureGeneratorCRC32.hpp"

std::shared_ptr<SignatureGenerator> SignatureGeneratorFactory::Create(const std::string &id)
{
    if (id == "crc32") {
        return std::make_shared<SignatureGeneratorCRC32>();
    } else if (id == "md5") {
        return std::make_shared<SignatureGeneratorMD5>();
    } else {
        throw std::runtime_error(std::string("Unknown signature generator id ") + id);
    }
}
