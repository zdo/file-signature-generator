#ifndef SIGNATURE_GENERATOR_FACTORY
#define SIGNATURE_GENERATOR_FACTORY

#include "SignatureGenerator.hpp"
#include <memory>

class SignatureGeneratorFactory {
public:
    static std::shared_ptr<SignatureGenerator> Create(const std::string &id);
};

#endif
