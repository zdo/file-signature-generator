#ifndef MEMORY_MAPPED_BLOCK_HPP
#define MEMORY_MAPPED_BLOCK_HPP

#include "MemoryBlock.hpp"

class MemoryMappedBlock : public MemoryBlock {
public:
    enum Access {
        Access_Read,
        Access_Write,
        Access_ReadWrite
    };

    MemoryMappedBlock(int fd, size_t offset, size_t size, Access access = Access_Read);
    virtual ~MemoryMappedBlock();

    void Sync();
    void CleanUp() override;

private:
    char *m_mmapData = NULL;
    size_t m_mmapOffset = 0;
    size_t m_mmapSize = 0;
};

#endif
