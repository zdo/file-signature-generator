#ifndef SIGNATURE_GENERATOR_MD5
#define SIGNATURE_GENERATOR_MD5

#include "../SignatureGenerator.hpp"

class SignatureGeneratorMD5 : public SignatureGenerator
{
    virtual size_t GetRequestedOutputBlockSize();
    virtual void Process(std::shared_ptr<MemoryBlock> input,
        std::shared_ptr<MemoryBlock> output);
};

#endif
