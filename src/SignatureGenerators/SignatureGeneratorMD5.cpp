#include "SignatureGeneratorMD5.hpp"
#include <openssl/md5.h>
#include <assert.h>

size_t SignatureGeneratorMD5::GetRequestedOutputBlockSize()
{
    return 16;
}

void SignatureGeneratorMD5::Process(std::shared_ptr<MemoryBlock> input,
    std::shared_ptr<MemoryBlock> output)
{
    assert(output->GetSize() >= 16);

    MD5((const unsigned char *)input->GetData(),
        input->GetSize(),
        (unsigned char *)output->GetData());
}
