#include "SignatureGeneratorCRC32.hpp"
#include <boost/crc.hpp>
#include <assert.h>
#include <string.h>
#include "../Logging.hpp"

size_t SignatureGeneratorCRC32::GetRequestedOutputBlockSize()
{
    return sizeof(uint32_t);
}

void SignatureGeneratorCRC32::Process(std::shared_ptr<MemoryBlock> input,
    std::shared_ptr<MemoryBlock> output)
{
    assert(output->GetSize() >= sizeof(uint32_t));

    boost::crc_32_type result;
    result.process_bytes(input->GetData(), input->GetSize());
    uint32_t crc32 = result.checksum();
    memcpy(output->GetData(), &crc32, sizeof(crc32));
}
