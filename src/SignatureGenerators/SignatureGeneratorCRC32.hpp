#ifndef SIGNATURE_GENERATOR_CRC32
#define SIGNATURE_GENERATOR_CRC32

#include "../SignatureGenerator.hpp"

class SignatureGeneratorCRC32 : public SignatureGenerator
{
    virtual size_t GetRequestedOutputBlockSize();
    virtual void Process(std::shared_ptr<MemoryBlock> input,
        std::shared_ptr<MemoryBlock> output);
};

#endif
