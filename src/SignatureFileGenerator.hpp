#ifndef SIGNATURE_FILE_GENERATOR
#define SIGNATURE_FILE_GENERATOR

#include <string>
#include <memory>
#include <vector>

#include "MemoryBlock.hpp"
#include "File.hpp"
#include "SignatureGenerator.hpp"

class SignatureFileGenerator {
public:
    SignatureFileGenerator(const std::string &inputFile, const std::string &outputFile);

    void Process();

    size_t GetBlockSize() const { return m_blockSize; }
    void SetBlockSize(size_t v) { m_blockSize = v; }

    void SetThreadsCount(size_t v) { m_threadsCount = v; }

    std::shared_ptr<SignatureGenerator> GetSignatureGenerator() const { return m_signatureGenerator; }
    void SetSignatureGenerator(const std::shared_ptr<SignatureGenerator> &v) { m_signatureGenerator = v; }

private:
    std::string m_inputFilePath, m_outputFilePath;
    size_t m_blockSize = 1024 * 1024;
    size_t m_threadsCount = 0;
    std::shared_ptr<SignatureGenerator> m_signatureGenerator;

    std::shared_ptr<File> m_inputFile, m_outputFile;

    size_t GetRecommendedThreadsCount() const;
    void PrepareToProcess();
    void ProcessWithThreads();
    void FinalizeProcessing();
    size_t GetTotalBlocksCount() const;

    void ThreadFn(size_t index, size_t totalThreadsCount);
};

#endif
