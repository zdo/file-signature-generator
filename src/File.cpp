#include "File.hpp"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdexcept>

#include "Logging.hpp"

File::File(const std::string &path, Access accessMode, TruncateMode truncMode, size_t truncateToSize)
{
    int modeInt = 0;
    int perm = 0;
    switch (accessMode) {
    case Access_ReadOnly:
        modeInt = O_RDONLY;
        break;
    case Access_WriteOnly:
        modeInt = O_WRONLY | O_CREAT;
        perm = S_IRUSR | S_IWUSR;
        break;
    case Access_ReadWrite:
        modeInt = O_RDWR | O_CREAT;
        perm = S_IRUSR | S_IWUSR;
        break;
    default:
        throw std::runtime_error(std::string("Invalid access mode"));
    }

    if (truncMode == TruncateMode_Yes) {
        modeInt |= O_TRUNC;
    }

    m_fd = open(path.c_str(), modeInt, perm);
    if (m_fd < 0) {
        throw std::runtime_error(std::string("Can't open file ") + path);
    }

    if (truncMode == TruncateMode_Yes) {
        ftruncate(m_fd, 0);
        ftruncate(m_fd, truncateToSize);
    }

    struct stat statInfo;
    fstat(m_fd, &statInfo);
    m_totalSize = statInfo.st_size;

    BOOST_LOG_TRIVIAL(debug) << "File=" << path
        << " truncateToSize=" << truncateToSize
        << " m_totalSize=" << m_totalSize;
}

File::~File()
{
    if (m_fd >= 0) {
        close(m_fd);
    }
}

std::shared_ptr<MemoryMappedBlock> File::MapToMemory(size_t offset, size_t size,
    MemoryMappedBlock::Access access)
{
    auto block = std::make_shared<MemoryMappedBlock>(m_fd, offset, size, access);
    return block;
}
