#include "Options.hpp"
#include <boost/program_options.hpp>
#include <exception>
#include <iostream>

#include "Logging.hpp"

namespace po = boost::program_options;

static size_t ParseBlockSize(const std::string &blockSizeStr)
{
    if (blockSizeStr.size() == 0) {
        throw std::invalid_argument("Empty block size");
    }

    char modifierChar = blockSizeStr[blockSizeStr.size() - 1];
    modifierChar = tolower(modifierChar);

    size_t modifier = 1;
    switch (modifierChar) {
    case 'k':
        modifier = 1024;
        break;
    case 'm':
        modifier = 1024 * 1024;
        break;
    case 'g':
        modifier = 1024 * 1024 * 1024;
        break;
    default:
        if (!isdigit(modifierChar)) {
            auto errorText = std::string("Invalid block size modifier character: ") + modifierChar;
            throw std::invalid_argument(errorText);
        }
    }

    bool isModifierApplied = (modifier != 1);
    auto withoutModifier = (isModifierApplied ?
        blockSizeStr.substr(0, blockSizeStr.size() - 1) :
        blockSizeStr);

    size_t blockSize = 0;
    try {
        blockSize = std::stoul(withoutModifier);
    } catch (const std::exception &exc) {
        auto errorText = std::string("Invalid block size: ") + exc.what();
        throw std::invalid_argument(errorText);
    }

    // In production we have to implement here an overflow detection.
    blockSize *= modifier;

    return blockSize;
}

Options Options::Parse(int argc, char **argv)
{
    Options options;

    po::options_description desc{"Options"};
    desc.add_options()
        ("help,h", "Help screen")
        ("input", po::value<std::string>()->required(), "Input file")
        ("output", po::value<std::string>()->required(), "Output signature file")
        ("block", po::value<std::string>()->default_value("1M"), "Block size (use may use optional suffixes like K,M,G)")
        ("threads", po::value<size_t>()->default_value(0), "How many threads to use (0 means taking a system's value)")
        ("hash", po::value<std::string>()->default_value("crc32"), "Hash function to use (possible values: crc32, md5)")
        ("verbose", "Be verbose")
        ("debug", "Print debugging information")
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);

    if (vm.count("help") || argc == 1) {
        std::cout << desc << std::endl;
        exit(0);
    }

    auto minLogLevel = boost::log::trivial::warning;
    if (vm.count("debug")) {
        minLogLevel = boost::log::trivial::debug;
    } else if (vm.count("verbose")) {
        minLogLevel = boost::log::trivial::info;
    }
    boost::log::core::get()->set_filter(boost::log::trivial::severity >= minLogLevel);

    po::notify(vm);

    options.inputFilePath = vm["input"].as<std::string>();
    options.signatureFilePath = vm["output"].as<std::string>();
    options.hashFunction = vm["hash"].as<std::string>();
    options.blockSize = ParseBlockSize(vm["block"].as<std::string>());
    options.threadsCount = vm["threads"].as<size_t>();
    BOOST_LOG_TRIVIAL(debug) << "Block size is set to " << options.blockSize << " bytes";

    return options;
}