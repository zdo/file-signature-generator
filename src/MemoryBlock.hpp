#ifndef MEMORY_BLOCK_HPP
#define MEMORY_BLOCK_HPP

#include <unistd.h>

class MemoryBlock {
public:
    MemoryBlock();
    MemoryBlock(size_t size);
    virtual ~MemoryBlock();

    void * GetData() const { return m_data; }
    size_t GetSize() const { return m_size; }

    virtual void CleanUp();

protected:
    void *m_data = NULL;
    size_t m_size = 0;
};

#endif
