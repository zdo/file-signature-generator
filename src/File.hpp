#ifndef FILE_HPP
#define FILE_HPP

#include <string>
#include <memory>
#include "MemoryMappedBlock.hpp"

class File {
public:
    enum Access {
        Access_ReadOnly,
        Access_WriteOnly,
        Access_ReadWrite
    };

    enum TruncateMode {
        TruncateMode_Yes,
        TruncateMode_No
    };

    File(const std::string &path, Access accessMode,
        TruncateMode truncMode = TruncateMode_No,
        size_t truncateToSize = 0);
    ~File();

    size_t GetTotalSize() const { return m_totalSize; }

    std::shared_ptr<MemoryMappedBlock> MapToMemory(size_t offset, size_t size,
        MemoryMappedBlock::Access access = MemoryMappedBlock::Access_Read);

private:
    int m_fd = -1;
    size_t m_totalSize;
};

#endif
