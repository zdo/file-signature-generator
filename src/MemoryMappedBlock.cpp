#include "MemoryMappedBlock.hpp"
#include <sys/mman.h>
#include <stdexcept>
#include <errno.h>
#include <string.h>
#include "Logging.hpp"

MemoryMappedBlock::MemoryMappedBlock(int fd, size_t offset, size_t size, Access access)
    : MemoryBlock()
{
    int accessFlag = 0;
    switch (access) {
    case Access_Read:
        accessFlag = PROT_READ;
        break;
    case Access_Write:
        accessFlag = PROT_WRITE;
        break;
    case Access_ReadWrite:
        accessFlag = PROT_READ | PROT_WRITE;
        break;
    }

    size_t pageSize = sysconf(_SC_PAGE_SIZE);
    BOOST_LOG_TRIVIAL(debug) << "pageSize=" << pageSize;

    size_t mmapOffset = (offset / pageSize) * pageSize;
    size_t mmapOffsetFromOrigin = (offset - mmapOffset);
    size_t mmapSize = size + mmapOffsetFromOrigin;
    void *mmapedData = mmap(NULL, mmapSize, accessFlag, MAP_SHARED, fd, mmapOffset);

    if (mmapedData != MAP_FAILED) {
        m_mmapData = (char *)mmapedData;
        m_mmapOffset = mmapOffset;
        m_mmapSize = mmapSize;

        m_data = (char *)mmapedData + mmapOffsetFromOrigin;
        m_size = size;

        BOOST_LOG_TRIVIAL(debug) << "mmap() succeeded:"
            << " offset=" << offset
            << " size=" << size
            << " fd=" << fd;
    } else {
        BOOST_LOG_TRIVIAL(debug) << "mmap() is failed:"
            << " mmapedData=" << mmapedData
            << " this->GetData()=" << this->GetData()
            << " fd=" << fd
            << " offset=" << offset
            << " size=" << size
            << " errno=" << strerror(errno);

        this->CleanUp();
        throw std::runtime_error("mmap error");
    }
}

MemoryMappedBlock::~MemoryMappedBlock()
{
    this->CleanUp();
}

void MemoryMappedBlock::CleanUp()
{
    if (m_mmapData != NULL) {
        BOOST_LOG_TRIVIAL(debug) << "munmap(" << m_data << ", " << m_size << ")";

        int status = munmap(m_mmapData, m_mmapSize);
        if (status != 0) {
            BOOST_LOG_TRIVIAL(debug) << "munmap failed: " << strerror(errno);
        }

        m_mmapData = NULL;
        m_mmapSize = 0;
        m_mmapOffset = 0;

        m_data = NULL;
        m_size = 0;
    }
}

void MemoryMappedBlock::Sync()
{
    msync(m_mmapData, m_mmapSize, MS_ASYNC);
}
