# Performance Analysis

This document contains several estimates and notes about performance of the application.

## Input file

The input file is a huge archive:

```bash
$ ls big-file -l
-rw-r--r-- 1 zdo zdo 12881825294 июн 25  2018 big-file
$ ls big-file -lh
-rw-r--r-- 1 zdo zdo 12G июн 25  2018 big-file
$ file big-file 
big-file: bzip2 compressed data, block size = 100k
```

## Configuration

The application was being launched on this configuration:

* CPU Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz (8 cores including HT)
* RAM 12G
* SSD
* Fedora 28 x86_64

## Low RAM requirements

1. The application's RAM requirements is very low:

    ```bash
    $ docker run --memory-swappiness 0 --memory 4M -ti -v $(pwd):/work test/generate-signature:latest --input /work/big-file --output /work/signature --block 10M --threads 8 --verbose --hash md5
    [2018-12-27 12:09:11.126245] [0x00007efee965afc0] [info]    Processing started...
    [2018-12-27 12:09:16.767609] [0x00007efee965afc0] [info]    The job took 5.64131 seconds
    ```

2. But if `bash` will be started first and only then the application - it would crash due to lack of RAM:

    ```bash
    $ docker run --memory-swappiness 0 --memory 4M -ti -v $(pwd):/work --entrypoint bash test/generate-signature:latest
    root@115315d44cc4:/app# ./generate-signature --input /work/big-file --output /work/signature --block 10M --threads 8 --verbose --hash md5
    [2018-12-27 12:10:46.278975] [0x00007ff293aecfc0] [info]    Processing started...
    Killed
    root@115315d44cc4:/app# dmesg
    ...
    [12366.599125] Task in /docker/115315d44cc4aa7fb991ab11fd5c077e71a9392650215d760e95a72ef22f22ad killed as a result of limit of /docker/115315d44cc4aa7fb991ab11fd5c077e71a9392650215d760e95a72ef22f22ad
    [12366.599128] memory: usage 4096kB, limit 4096kB, failcnt 58402
    [12366.599129] memory+swap: usage 4096kB, limit 8192kB, failcnt 0
    [12366.599130] kmem: usage 3000kB, limit 9007199254740988kB, failcnt 0
    [12366.599130] Memory cgroup stats for /docker/115315d44cc4aa7fb991ab11fd5c077e71a9392650215d760e95a72ef22f22ad: cache:48KB rss:784KB rss_huge:0KB shmem:0KB mapped_file:0KB dirty:0KB writeback:0KB swap:0KB inactive_anon:856KB active_anon:188KB inactive_file:20KB active_file:24KB unevictable:0KB
    [12366.599139] [ pid ]   uid  tgid total_vm      rss pgtables_bytes swapents oom_score_adj name
    [12366.599264] [12812]     0 12812     4627      486    90112        0             0 bash
    [12366.599265] [12867]     0 12867   177278      730   327680        0             0 generate-signat
    [12366.599267] Memory cgroup out of memory: Kill process 12867 (generate-signat) score 791 or sacrifice child
    [12366.599277] Killed process 12867 (generate-signat) total-vm:709112kB, anon-rss:552kB, file-rss:2368kB, shmem-rss:0kB
    [12366.599672] oom_reaper: reaped process 12867 (generate-signat), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
    ```

3. It's interesting that if we start `sh` instead of `bash` - it is going to work:

    ```bash
    $ docker run --memory-swappiness 0 --memory 4M -ti -v $(pwd):/work --entrypoint sh test/generate-signature:latest
    # ./generate-signature --input /work/big-file --output /work/signature --block 10M --threads 8 --verbose --hash md5
    [2018-12-27 12:13:49.906179] [0x00007f1e9c494fc0] [info]    Processing started...
    [2018-12-27 12:13:56.408839] [0x00007f1e9c494fc0] [info]    The job took 6.50254 seconds
    #
    ```

4. If `bash` and `--memory=8M` (instead of `--memory=4M`) is specified - it works fine too:

    ```bash
    $ docker run --memory-swappiness 0 --memory 8M -ti -v $(pwd):/work --entrypoint bash test/generate-signature:latest
    root@994cd01cde29:/app# ./generate-signature --input /work/big-file --output /work/signature --block 10M --threads 8 --verbose --hash md5
    [2018-12-27 12:15:05.617964] [0x00007f6c12fe4fc0] [info]    Processing started...
    [2018-12-27 12:15:10.209678] [0x00007f6c12fe4fc0] [info]    The job took 4.5916 seconds
    root@994cd01cde29:/app#
    ```

## One thread vs parallel

* Parallel execution with default threads count (8 for this configuration):

    ```bash
    $ docker run -ti -v $(pwd):/work test/generate-signature:latest --input /work/big-file --output /work/signature --block 10M --verbose --hash md5
    [2018-12-27 12:23:21.234196] [0x00007f1f99c3dfc0] [info]    Processing started...
    [2018-12-27 12:23:25.207452] [0x00007f1f99c3dfc0] [info]    The job took 3.9728 seconds
    ```

    ![htop|small](./htop.jpg)

* Single thread only:

    ```bash
    $ docker run -ti -v $(pwd):/work test/generate-signature:latest --input /work/big-file --output /work/signature --block 10M --verbose --hash md5 --threads 1
    [2018-12-27 12:23:32.272266] [0x00007f9846b34fc0] [info]    Processing started...
    [2018-12-27 12:23:52.117540] [0x00007f9846b34fc0] [info]    The job took 19.845 seconds
    ```

## RAM limits

The removing RAM limits boosts the app.

* With 4MB limit:

    ```bash
    $ docker run --memory-swappiness 0 --memory 4M -ti -v $(pwd):/work test/generate-signature:latest --input /work/big-file --output /work/signature --block 10M --threads 8 --verbose --hash md5
    [2018-12-27 12:19:16.527886] [0x00007ff997224fc0] [info]    Processing started...
    [2018-12-27 12:19:22.198730] [0x00007ff997224fc0] [info]    The job took 5.67077 seconds
    ```

* Without limit:

    ```bash
    $ docker run -ti -v $(pwd):/work test/generate-signature:latest --input /work/big-file --output /work/signature --block 10M --threads 8 --verbose --hash md5
    [2018-12-27 12:19:28.326052] [0x00007f31a1c3ffc0] [info]    Processing started...
    [2018-12-27 12:19:32.270724] [0x00007f31a1c3ffc0] [info]    The job took 3.94456 seconds
    ```
