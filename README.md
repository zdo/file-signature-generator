# File Signature Generator

The utility generates a signature file from some input file.

Features:
* reading the input file is performed chunk per chunk of fixed size (see `--block` argument)
* input and output is being done by `mmap`, so it is incredibly fast
* processing is being executed on multiple threads simultaneously
* consult `--help` if you stuck

## Performance

There is some [PERFORMANCE ANALYSIS](./performance-analysis/README.md).

## Usage

In this simple example a block size is 4 bytes.

```bash
$ echo -n 'somedata' > /tmp/input
$ echo -n 'some' | md5sum
03d59e663c1af9ac33a9949d1193505a  -
$ echo -n 'data' | md5sum
8d777f385d3dfec8815d20f7496026dc  -
$ ./generate-signature --input /tmp/input --output /tmp/output --block 4 --hash md5
$ xxd /tmp/output
00000000: 03d5 9e66 3c1a f9ac 33a9 949d 1193 505a  ...f<...3.....PZ
00000010: 8d77 7f38 5d3d fec8 815d 20f7 4960 26dc  .w.8]=...] .I`&.
```

## Docker

Build docker image:

```bash
$ ./docker-build
```

Run:

```bash
$ docker run -ti -v /tmp:/data test/generate-signature:latest --input /data/input --output /data/output --block 4
$ xxd /tmp/output
00000000: 03d5 9e66 3c1a f9ac 33a9 949d 1193 505a  ...f<...3.....PZ
00000010: 8d77 7f38 5d3d fec8 815d 20f7 4960 26dc  .w.8]=...] .I`&.
$
```
